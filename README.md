# The Good Dogs Project

A Hugo-based site using Bryan's theme for the Good Docs Project's Git training workshop.

Hosted on Netlify: [https://2023-july-salt-training.netlify.app/](https://2023-july-salt-training.netlify.app/)

Additional helpful links:

- [Training agenda](https://2023-july-salt-training.netlify.app/agenda/)
- [Git cheat sheet](https://gitlab.com/tgdp/git-training/curriculum/-/blob/main/git-cheat-sheet.md)
- [Homework](https://2023-july-salt-training.netlify.app/homework/)
